/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.shapeabstract;

/**
 *
 * @author EAK
 */
public class TestShape {

    public static void main(String[] args) {
        Circle circle = new Circle(5);
        System.out.println(circle);
        Rectangle rectangle = new Rectangle(3, 5);
        System.out.println(rectangle);
        Square square = new Square(4);
        System.out.println(square);
        Triangle triangle = new Triangle(2, 4);
        System.out.println(triangle);

        Shape[] shapes = {circle, rectangle, square, triangle};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].getName() + " Area: " + shapes[i].calArea());
        }
    }
}
